public class Month {

    private String name;
    private int number;
    private int days;

    public Month(String name, int number, int days) {
        this.name = name;
        this.number = number;
        this.days = days;
    }

    @Override
    public String toString() {
        return "Month " +
                "name: " + name + '\'' +
                ", number: " + number +
                ", days: " + days;
    }
}
