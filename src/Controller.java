import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Controller {

    public static final String FILE_PATH = "data/calendar.txt";
    public static final String CHARSET = "UTF-8";
    public static final String FILE_TO_WRITE_INTO = "src/data/writeToThisFile.txt";

    List<Month> monthList = new ArrayList<>();

    public void start(){
        readFile();
        printData();
        writeIntoFile();
    }

    private void printData() {
        for (Month month : monthList) {
            System.out.println(month);
        }
    }

    private void readFile() {
        InputStream inputStream = getClass()
                .getResourceAsStream(FILE_PATH);

        Scanner scanner = new Scanner(inputStream, CHARSET);

        while (scanner.hasNextLine()) {

            String line = scanner.nextLine();

            if (!line.isEmpty()) {
                String data[] = line.split("-");

                String name = data[0].trim();
                int number = Integer.parseInt(data[1].trim());

                String numOfDays = data[2]
                        .replace("days", "");

                int days = Integer.parseInt(numOfDays.trim());

                Month month;
                if (data.length > 3) {
                    String type = data[3];
                    month = new HolidayMonth(name, number, days, type);
                } else {
                    month = new Month(name, number, days);
                }

                monthList.add(month);
            }

        }

    }

    private void writeIntoFile() {

        BufferedWriter bufferedWriter;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(FILE_TO_WRITE_INTO));

            for (Month month : monthList) {
                bufferedWriter.write(month.toString());
                bufferedWriter.newLine();
            }

            bufferedWriter.flush();
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error!");
        }
    }

}
