public class HolidayMonth extends Month{

    private String type;

    public HolidayMonth(String name, int number, int days, String type) {
        super(name, number, days);
        this.type = type;
    }
}
